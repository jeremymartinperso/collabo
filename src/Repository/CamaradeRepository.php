<?php

namespace App\Repository;

use App\Entity\Camarade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Camarade|null find($id, $lockMode = null, $lockVersion = null)
 * @method Camarade|null findOneBy(array $criteria, array $orderBy = null)
 * @method Camarade[]    findAll()
 * @method Camarade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CamaradeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Camarade::class);
    }

    // /**
    //  * @return Camarade[] Returns an array of Camarade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Camarade
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
