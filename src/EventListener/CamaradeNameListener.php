<?php
namespace App\EventListener;

use App\Entity\Camarade;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class CamaradeNameListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof Camarade) {
            $camaradeName = $args->getObject()->getNom();

            if (
                !strpos($camaradeName, 'stein') &&
                !strpos($camaradeName, 'berg')
            ) {
                throw new SyntaxError('Un nom juif doit être constitué des suffixes \'berg\', \'stein\', \'cohen\', \'benichou\', \'levy\' ou \'zemmour\'');
            }
        }
    }

}