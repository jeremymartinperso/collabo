<?php

namespace App\Form;

use App\Entity\Camarade;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CamaradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nom',
                TextType::class,
                array(
                    'label' => 'Nom'
                )
            )
            ->add(
                'prenom',
                TextType::class,
                array(
                    'label' => 'Prénom')
            )
            ->add(
                'age',
                IntegerType::class,
                array(
                    'label' => 'Age'
                )
            )
            ->add(
                'adresse',
                TextType::class,
                array(
                    'label' => 'Adresse'
                )
            )
            ->add(
                'save',
                SubmitType::class, array('label' => 'Je dénonce ce camarade', 'attr' => array('class' => 'btn btn-outline-success')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Camarade::class,
        ]);
    }
}
