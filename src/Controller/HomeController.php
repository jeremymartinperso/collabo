<?php

namespace App\Controller;

use App\Entity\Camarade;
use App\Form\CamaradeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $camaradesAuCamps = $entityManager->createQueryBuilder()
            ->select('COUNT(camarades)')
            ->from(Camarade::class,'camarades')->getQuery()->getSingleScalarResult();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'countCamaradesAuCamps' => $camaradesAuCamps
        ]);
    }

    /**
     * @Route("/heuresSombres", name="sombre")
     */
    public function heuresSombresAction (Request $request) {
        $camarade = new Camarade();
        $form = $this->createForm(CamaradeType::class, $camarade);
        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($camarade);
            $entityManager->flush();
            return $this->redirectToRoute('home');
        }

        return $this->render('home/heuresSombres.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/sendEmail", name="sendEmail")
     */
    public function sendEmail (Request $request, \Swift_Mailer $mailer) {
        $content = $request->get('content');
        $message = (new \Swift_Message('Un message de soutien pour toi'))
            ->setFrom('collabo@gmail.com')
            ->setTo('jeremymartinsio1@gmail.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/registration.html.twig',
                    array('content' => $content)
                ),
                'text/html'
            );
        $mailer->send($message);

        return $this->redirectToRoute('home');
    }
}
